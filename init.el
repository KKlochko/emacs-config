;;;
;;
;; Copyright (C) 2022-2024 Kostiantyn Klochko <kklochko@protonmail.com>
;;
;; This file is part of emacs-config
;;
;; emacs-config is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; emacs-config is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with emacs-config. If not, see
;; <https://www.gnu.org/licenses/>.
;;
;;; Code:

;; built-in
(require 'package)

(add-to-list 'package-archives '("gnu"   . "https://elpa.gnu.org/packages/"))

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)

;; Hide Welcome screen
(setq inhibit-startup-screen t)

(use-package all-the-icons
  :ensure t)

;; Set the theme
(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
	doom-themes-enable-italic t  ; if nil, italics is universally disabled
	doom-theme 'doom-nord)       ; change doom-theme to doom-nord
  (load-theme 'doom-nord t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-nord") ; use "doom-colors" for less minimal icon theme

  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

(when (version<= "26.0.50" emacs-version )
  (setq display-line-numbers-type 'relative)
  (global-display-line-numbers-mode))

;; Disable the tool-bar
(tool-bar-mode -1)

;; Disable the scroll-bar
(toggle-scroll-bar -1)

(use-package multiple-cursors
  :ensure t
  :bind (("C-S-c C-S-c" . mc/edit-lines)
	 ("C->" . mc/mark-next-like-this)
	 ("C-<" . mc/mark-previous-like-this)
	 ("C-c C-<" . mc/mark-all-like-this)))

(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode 1))

(use-package magit
  :ensure t)

;; built-in
;; Add a buffer which shows information about opened buffers
(require 'bs)
(setq bs-configurations
  '(("files" "^\\*scratch\\*" nil nil bs-visits-non-file bs-sort-buffer-interns-are-last)))
(global-set-key (kbd "<f2>") 'bs-show)

;; built-in
(require 'ido)
(ido-mode t)
(setq ido-enable-flex-matching t)

;; Windows moving
;; Using plugin: windmove (build-in)
(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)

(global-set-key (kbd "C-c h") 'windmove-left)
(global-set-key (kbd "C-c j") 'windmove-down)
(global-set-key (kbd "C-c k") 'windmove-up)
(global-set-key (kbd "C-c l") 'windmove-right)

(use-package neotree
  :ensure t
  :bind (("<f12>" . neotree-toggle)))

;; build-in
;; Bind for org-agenda
(global-set-key (kbd "C-x a a") 'org-agenda)

;; Org-roam
(use-package org-roam
       :ensure t
       :init
       (setq org-roam-v2-ack t)
       :custom
       ;; You must be sure that the folders are exist.
       ;; Change the path to the org-roam directory to your path.
       (org-roam-directory "~/org-roam/")
       :bind (("C-c n l" . org-roam-buffer-toggle)
	      ("C-c n f" . org-roam-node-find)
	      ("C-c n i" . org-roam-node-insert)
	      ;;org-roam-dailies
	      ("C-c d y" . org-roam-dailies-goto-yesterday)
	      ("C-c d t" . org-roam-dailies-goto-today)
	      ("C-c d n" . org-roam-dailies-goto-tomorrow)
	      ("C-c d d" . org-roam-dailies-goto-date)

	      ("C-c d c y" . org-roam-dailies-capture-yesterday)
	      ("C-c d c t" . org-roam-dailies-capture-today)
	      ("C-c d c n" . org-roam-dailies-capture-tomorrow)
	      ("C-c d c d" . org-roam-dailies-capture-date))
       :config
       (org-roam-setup))

;; UI for graph
(use-package org-roam-ui
  :ensure t)

;; Change * -> bullets
(use-package org-bullets
	 :ensure t
	 :config
	 (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
	 (setq org-hide-emphasis-markers t)
	 (font-lock-add-keywords 'org-mode
				 '(("^ *\\([-]\\) "
				    (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•")))))))

(use-package vterm
  :ensure t)

(use-package swiper-helm
  :ensure t
  ;; It (C-f) replace move-forward binding!!!
  ;; You can replace C-S, C-R.
  :bind ("C-f" . swiper))

(use-package popper
  :ensure t
  :bind (("M-`"     . popper-toggle-latest)
	 ("M-~"     . popper-cycle)
	 ("C-x M-`" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
	'("\\*Messages\\*"
	  "\\*Warnings\\*"
	  "\\*Backtrace\\*"
	  "\\*compilation\\*"
	  "Output\\*$"
	  "\\*undo-tree\\*" ;; For Undo-tree mode buffer
	  ;; terms and shells
	  "\\*Async Shell Command\\*"
	  "\\*vterm\\*"
	  "\\*shell\\*"
	  "\\*eshell\\*"
	  ;; Alchemist buffers
	  "\\*Alchemist-IEx\\*"
	  "\\*alchemist test report\\*"
	  ;; For org roam buffers
	  "\\*helm-mode-org-roam-node-insert\\*"
	  "\\*helm-mode-org-roam-node-find\\*"
	  ;; Others
	  help-mode
	  compilation-mode))
  (popper-mode +1)
  :config
  ;; For echo-area hints
  (require 'popper-echo)
  (popper-echo-mode +1))

(setq prettify-symbols-alist
    '(("lambda" . ?λ)
      ("pi" . ?π)
      ("M_PI" . ?π)
      ("alpha" . ?α)
      ("beta" . ?β)
      ("gamma" . ?γ)
      ("delta" . ?Δ)
      ("epsilon" . ?ε)
      ("theta" . ?θ)
      ("psi" . ?φ)
      ("||" . ?∨)
      ("&&" . ?∧)
      ("->" . ?→)
      ("<-" . ?←)
      ("=>" . ?⇒)
      ("==" . ?=)
      ("+-" . ?±)
      ("map" . ?↦)
      ("!=" . ?≠)
      ("/=" . ?≠)
      ("<=" . ?≤)
      (">=" . ?≥)
      ("nil" . ?∅)
      ("null" . ?∅)
      ("NULL" . ?∅)
      ("None" . ?∅)
      ("sqrt" . ?√)
      ("sum" . ?∑)
      ("**2" . ?²)
      ("^2" . ?²)
      ("**3" . ?³)
      ("^3" . ?³)
      ("**n" . ?ⁿ)
      ("^n" . ?ⁿ)))
(global-prettify-symbols-mode 1)

(use-package evil
  :ensure t
  :init
  (setq evil-want-C-u-scroll t)
  :config
  (evil-mode 1)
  ;; Tab works in org-mode as org-cycle
  (evil-define-key 'normal org-mode-map (kbd "<tab>") #'org-cycle))

(use-package evil-collection
  :ensure t
  :after evil
  :config
  (setq evil-want-integration t)
  (evil-colection-init))

(use-package evil-snipe
  :ensure t
  :config
  (evil-snipe-mode +1)
  (evil-snipe-override-mode +1))

(use-package evil-multiedit
  :ensure t
  :config
  (evil-multiedit-default-keybinds))

(use-package web-beautify
  :ensure t)

(use-package reverse-im
  :ensure t
  :custom
  (reverse-im-input-methods '("ukrainian-computer"))
  :config
  (reverse-im-mode t))

(use-package lorem-ipsum
  :ensure t)

(use-package ace-window
  :ensure t
  :config
  (global-set-key (kbd "M-o") 'ace-window))

(use-package texfrag
  :ensure t
  :hook ((org-mode . texfrag-mode)))

(use-package lsp-mode
  :ensure t
  :init
  ;; Set prefix for lsp-command-keymap
  (setq lsp-keymap-prefix "C-c l")
  :hook ((lsp-mode . lsp-enable-which-key-integration)
	 (rust-mode . lsp)
	 (go-mode . lsp)
	 (racket-mode . lsp)
	 ;; ts-ls
	 (js-mode . lsp)
	 (ts-mode . lsp)
	 ;; html-ls
	 (html-mode . lsp)
	 (css-mode . lsp))
  :config
  (require 'dap-cpptools)
  (yas-global-mode))

(use-package ccls
  :ensure t
  :hook ((c-mode c++-mode objc-mode cuda-mode) . lsp))

(use-package lsp-pyright
  :ensure t
  :hook (python-mode . lsp)
  :config
  ;; To get typechecking working properly
  ;; set this to nil if getting too many false positive type errors
  (setq lsp-pyright-use-library-code-for-types t)
  (setq lsp-pyright-stub-path (concat (getenv "HOME") "/Prog/Python/python-type-stubs")))

(use-package lsp-java
  :ensure t
  :hook ((java-mode-hook . lsp)))

(use-package lsp-treemacs
  :ensure t)

(use-package dap-mode
  :ensure t
  :after lsp-mode
  :config
  (require 'dap-java)
  (require 'dap-python)
  (require 'dap-cpptools)
  (dap-cpptools-setup)
  (dap-auto-configure-mode)
  (dap-mode 1)
  (dap-ui-mode 1)
  ;; enables mouse hover support
  (dap-tooltip-mode 1)
  (tooltip-mode 1)
  ;; displays floating panel with debug buttons
  (dap-ui-controls-mode 1))

(use-package projectile
  :ensure t)

(use-package hydra
  :ensure t)

(use-package flycheck
  :ensure t)

(use-package avy
  :ensure t)

;; For company-mode in all buffers
(use-package company
  :ensure t
  :hook (after-init-hook . global-company-mode))
;; (add-hook 'after-init-hook 'global-company-mode)

(use-package helm-lsp
  :ensure t)

(use-package helm-xref
  :ensure t)

(use-package helm
  :ensure t
  :config
  (helm-mode)
  (require 'helm-xref)
  ;;(global-set-key (kbd "M-x") #'helm-M-x)
  ;;(global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
  ;;(global-set-key (kbd "C-x C-f") #'helm-find-files)
  (define-key global-map [remap find-file] #'helm-find-files)
  (define-key global-map [remap execute-extended-command] #'helm-M-x)
  (define-key global-map [remap switch-to-buffer] #'helm-mini))

(use-package which-key
  :ensure t
  :config
  (which-key-mode))

(use-package clojure-mode
  :ensure t)

(use-package cmake-mode
  :ensure t)

(use-package crystal-mode
  :ensure t)

(use-package dart-mode
  :ensure t)

;; Docker syntax support
(use-package dockerfile-mode
  :ensure t)

(use-package docker-compose-mode
  :ensure t)

(use-package ess
  :ensure t)

(use-package elixir-mode
  :ensure t)

;; The Elixir toolkit
(use-package alchemist
  :ensure t)

(use-package go-mode
  :ensure t)

(use-package haskell-mode
  :ensure t)

(use-package haxe-mode
  :ensure t)

(use-package json-mode
  :ensure t)

(use-package js2-mode
  :ensure t)

(use-package kotlin-mode
  :ensure t)

;; Shows the entire expression enclosed by the parens
(setq show-paren-style 'expression)
;; Visualization of matching parens
(show-paren-mode 2)

(use-package lua-mode
  :ensure t)

(use-package nim-mode
  :ensure t)

(use-package nix-mode
  :ensure t)

;; ocaml-mode
(use-package tuareg
  :ensure t)

(use-package ocamlformat
  :ensure t)

;; set TODO keywords
(setq org-todo-keywords
      (quote ((sequence  "TODO(t)" "NEXT(n)" "WAITING(w)" "DOING(d)" "|" "DONE(d)" "CANCELED(c)") )))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((scheme . t)
   (emacs-lisp . t)
   (R . t)
   (python . t)
   (shell . t)))

(use-package php-mode
  :ensure t)

(use-package qml-mode
  :ensure t)

(use-package racket-mode
  :ensure t)

(use-package rust-mode
  :ensure t)

(use-package toml-mode
  :ensure t)

(use-package v-mode
  :ensure t)

(use-package vala-mode
  :ensure t)

(use-package yaml-mode
  :ensure t)

(use-package zencoding-mode
  :ensure t
  :hook (sgml-mode . zencoding-mode))

(use-package zig-mode
  :ensure t)
